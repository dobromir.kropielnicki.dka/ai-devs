import { pipe } from 'fp-ts/function';
import { match } from 'fp-ts/Either';
import { taskFlow } from '@core/taskFlow.ts';
import { config, readCliParams } from './config.ts';
import { pickTaskByName } from '@core/pickTaskByName.ts';

const cliParams = readCliParams();
const taskToExecute = pickTaskByName(cliParams.task);

pipe(
  await taskFlow(taskToExecute)(),
  match(
    error => console.error(`❌ fail - ${error.message}:`, error.details),
    () => console.log('success ✅'),
  ),
);
