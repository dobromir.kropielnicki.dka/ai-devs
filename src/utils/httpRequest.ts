import { flatMap, of, TaskEither } from 'fp-ts/TaskEither';
import { AppError, appErrorTE } from '@core/types.ts';
import { pipe } from 'fp-ts/function';
import { tryCatch } from '@utils/fpHelpers.ts';
import { isRecord } from '@utils/record.ts';

export const httpPost = <T>(
  url: string,
  payload: T,
  options: { name: string; headers?: Record<string, string>; formData?: boolean },
): TaskEither<AppError, unknown> => {
  const { name, headers, formData } = options;
  return pipe(
    tryCatch(
      () =>
        fetch(url, {
          method: 'POST',
          body: formData ? toFormData(payload) : JSON.stringify(payload),
          headers,
        }),
      name,
    ),
    flatMap(getJson(name)),
  );
};

export const httpGet = (url: string, options: { name: string }): TaskEither<AppError, unknown> => {
  const { name } = options;
  return pipe(
    tryCatch(() => fetch(url), name),
    flatMap(checkResponseStatus(name)),
    flatMap(getJson(name)),
  );
};

const checkResponseStatus =
  (name: string) =>
  (response: Response): TaskEither<AppError, Response> => {
    return response.ok
      ? of(response)
      : appErrorTE<Response>(`response to ${name} ended up with error`, response.status);
  };

const getJson =
  (name: string) =>
  (response: Response): TaskEither<AppError, unknown> =>
    tryCatch(() => response.json(), name);

const toFormData = (value: unknown) => {
  const formData = new FormData();
  if (isRecord(value)) {
    Object.keys(value)
      .filter(key => value[key] !== undefined)
      .forEach(key => {
        formData.append(key, `${value[key]}`);
      });
  }
  return formData;
};
