import { Either, mapLeft, tryCatch, flatMap } from 'fp-ts/Either';
import { appError, AppError } from '@core/types.ts';
import { pipe } from 'fp-ts/function';
import { Validation } from 'io-ts';
import { fromEither, TaskEither } from 'fp-ts/TaskEither';

export const decode = <T>(
  value: unknown,
  validator: (val: unknown) => Validation<T>,
  errorMessage = 'unrecognized data',
): Either<AppError, T> =>
  pipe(
    validator(value),
    mapLeft(error => appError(errorMessage, { value, error })),
  );

export const parseTE = <T>(
  value: unknown,
  validator: (val: unknown) => Validation<T>,
  errorMessage = 'unrecognized data',
): TaskEither<AppError, T> => fromEither(decode(value, validator, errorMessage));

export const parseJson = <T>(
  value: string,
  validator: (val: unknown) => Validation<T>,
  errorMessage = 'unrecognized data',
): TaskEither<AppError, T> =>
  fromEither(
    pipe(
      tryCatch(
        () => JSON.parse(value) as unknown,
        () => appError('invalid json', value),
      ),
      flatMap(result => decode(result, validator, errorMessage)),
    ),
  );
