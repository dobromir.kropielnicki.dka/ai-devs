import { taskEither as TE } from 'fp-ts';
import { LazyArg, pipe } from 'fp-ts/function';
import { appError, AppError } from '@core/types.ts';
import { TaskEither } from 'fp-ts/TaskEither';
import { getOrElseW } from 'fp-ts/Either';

export const tap =
  <A, B>(sideEffect: (value: B) => void) =>
  (te: TE.TaskEither<A, B>): TE.TaskEither<A, B> => {
    return pipe(
      te,
      TE.map((result: B) => {
        sideEffect(result);
        return result;
      }),
    );
  };

export const log =
  <E, T>(message?: string) =>
  (te: TE.TaskEither<E, T>): TE.TaskEither<E, T> =>
    pipe(
      te,
      tap(value => {
        const logs = message ? [`${message}:`, value] : [value];
        console.info(...logs);
      }),
    );

export const tryCatch = <T>(
  action: LazyArg<Promise<T>>,
  actionName: string,
): TE.TaskEither<AppError, T> => {
  return TE.tryCatch<AppError, T>(action, (reason: unknown) =>
    appError(`${actionName} - unknown error`, reason),
  );
};

export const execute = async <T>(action: TaskEither<AppError, T>) => {
  return pipe(
    await action(),
    getOrElseW(error => {
      console.error(error);
      return Promise.reject(error);
    }),
  );
};
