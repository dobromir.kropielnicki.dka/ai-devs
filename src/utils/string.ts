import { isNotNil } from '@utils/nil';

export const isNotEmpty = (value: unknown): value is string => {
  return isNotNil(value) && isString(value) && value.trim() !== '';
};

export const isString = (value: unknown): value is string => {
  return typeof value === 'string' || value instanceof String;
};
