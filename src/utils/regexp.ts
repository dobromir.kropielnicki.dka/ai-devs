import { isNil } from '@utils/nil.ts';

export const findUrl = (message: string): string | undefined => {
  const urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gi;
  const matches = message.match(urlRegex);
  return isNil(matches) ? undefined : matches[0];
};
