export const isNil = (value: unknown): value is null | undefined => value === undefined || value === null;
export const isNotNil = <T>(value: T | undefined | null): value is T => value !== undefined && value !== null;
