import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';

const { API_KEY, API_URL } = process.env;
if (!API_KEY || !API_URL) {
  console.error('API_KEY and API_URL must be set');
  process.exit(-1);
}

export const config = {
  apiKey: API_KEY,
  apiUrl: API_URL,
};

export type CliParams = {
  task: string;
};

export const readCliParams = (): CliParams =>
  yargs(hideBin(process.argv))
    .options({
      task: { type: 'string', demandOption: true, describe: 'task to execute', alias: 't' },
    })
    .parseSync();
