import { helloApi } from '@exercise/helloApi.ts';
import { moderation } from '@exercise/moderation.ts';
import { blogger } from '@exercise/blogger.ts';
import { liar } from '@exercise/liar.ts';
import { inPrompt } from '@exercise/inPrompt.ts';
import { embedding } from '@exercise/embedding.ts';
import { whisper } from '@exercise/whisper.ts';

import { Exercise } from '@core/types.ts';
import { functions } from '@exercise/functions.ts';
import { rodo } from '@exercise/rodo.ts';
import { scraper } from '@exercise/scraper.ts';
import { whoAmI } from '@exercise/whoAmI.ts';
import { search } from '@exercise/search.ts';
import { people } from '@exercise/people.ts';
import { knowledge } from '@exercise/knowledge.ts';
import { tools } from '@exercise/tools.ts';
import { gnome } from '@exercise/gnome.ts';
import { ownApi } from '@exercise/ownapi.ts';
import { ownApiPro } from '@exercise/ownapipro.ts';
import { meme } from '@exercise/meme.ts';
import { optimalDb } from '@exercise/optimalDb.ts';
import { google } from '@exercise/google.ts';
import { md2html } from '@exercise/md2html.ts';

export const pickTaskByName = (name: string): Exercise<string | number[] | string[] | unknown> => {
  const allTasks = [
    helloApi,
    moderation,
    blogger,
    liar,
    inPrompt,
    embedding,
    whisper,
    functions,
    rodo,
    scraper,
    whoAmI,
    search,
    people,
    knowledge,
    tools,
    gnome,
    ownApi,
    ownApiPro,
    meme,
    optimalDb,
    google,
    md2html,
  ];
  return allTasks.find(task => task.name === name) ?? allTasks[0];
};
