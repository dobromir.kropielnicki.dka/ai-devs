import { left, TaskEither } from 'fp-ts/TaskEither';
import * as t from 'io-ts';

export type ExerciseAction<T> = (
  task: unknown,
  token: string,
) => TaskEither<AppError, T | { answer: T; token: string }>;

export type Exercise<T> = {
  action: ExerciseAction<T>;
  name: string;
};

export type AppError = {
  message: string;
  details?: unknown;
};

export const appError = (message: string, details?: unknown): AppError => {
  return details ? { message, details } : { message };
};

export const appErrorTE = <T>(message: string, details?: unknown): TaskEither<AppError, T> => {
  return left(appError(message, details));
};

export const AnswerWithTokens = t.type({ answer: t.unknown, token: t.string });
