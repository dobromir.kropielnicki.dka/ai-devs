import { flatMap, fromOption, map, of, TaskEither } from 'fp-ts/TaskEither';
import { appError, AppError, Exercise } from '@core/types.ts';
import { ChatOpenAI } from 'langchain/chat_models/openai';
import { ChatPromptTemplate } from 'langchain/prompts';
import { LLMChain } from 'langchain/chains';
import { pipe } from 'fp-ts/function';
import { tryCatch } from '@utils/fpHelpers.ts';
import { parseJson } from '@utils/decode.ts';
import * as t from 'io-ts';
import { HumanMessage, SystemMessage } from 'langchain/schema';
import { fromNullable } from 'fp-ts/Option';
import { googleIt, GoogleResult } from '@infrastructure/serpiapi';

const apiEndpoint = `${process.env.DK_API_URL}/${process.env.DK_API_SECRET}/google`;

export const google: Exercise<string> = {
  name: 'google',
  action: () => of(apiEndpoint),
};

export const googleUseCase = (question: string): TaskEither<AppError, string> => {
  return pipe(createGoogleQuery(question), flatMap(googleIt), flatMap(takeFirstLink));
};

const createGoogleQuery = (question: string): TaskEither<AppError, string> => {
  console.log(`creating query for: ${question}`);
  const llm = new ChatOpenAI({ modelName: 'gpt-3.5-turbo-1106', temperature: 0 });
  const prompt = ChatPromptTemplate.fromMessages([systemMessage, new HumanMessage(question)]);
  const llmChain = new LLMChain({ llm, prompt });

  return pipe(
    tryCatch(() => llmChain.call({}), 'ask for google query'),
    flatMap(({ text }) => parseJson(text, t.type({ query: t.string }).decode)),
    map(({ query }) => query),
  );
};

const systemMessage = new SystemMessage(`
Your job is to transform given text into a perfect query, which will be used for googling. 
Structure your answer as {"query": "text_send_to_google_to_get_the_best_answer"}
`);

const takeFirstLink = (result: GoogleResult): TaskEither<AppError, string> => {
  return fromOption(() => appError('link not found'))(
    fromNullable(result.organic_results[0]?.link),
  );
};
