import { AppError, Exercise } from '@core/types.ts';
import { flatMap, fromEither, map, sequenceArray, TaskEither } from 'fp-ts/TaskEither';
import { pipe } from 'fp-ts/function';
import { httpGet } from '@utils/httpRequest.ts';
import { decode } from '@utils/decode.ts';
import * as t from 'io-ts';
import { ChatOpenAI } from 'langchain/chat_models/openai';
import { ChatPromptTemplate } from 'langchain/prompts';
import { LLMChain } from 'langchain/chains';
import { tryCatch } from '@utils/fpHelpers.ts';

export const optimalDb: Exercise<string> = {
  name: 'optimaldb',
  action: (task: unknown) =>
    pipe(
      getDatabaseUrl(task),
      flatMap(fetchDatabase),
      flatMap(records => sequenceArray(records.map(addSummary))),
      map(combineIntoContextString),
    ),
};

const getDatabaseUrl = (task: unknown): TaskEither<AppError, string> =>
  pipe(
    fromEither(decode(task, t.type({ database: t.string }).decode)),
    map(({ database }) => database),
  );

type DbRecord = { name: string; info: string[] };
const fetchDatabase = (url: string): TaskEither<AppError, DbRecord[]> => {
  console.log(`get knowledge from ${url}`);
  const DbRawType = t.record(t.string, t.array(t.string));
  return pipe(
    httpGet(url, { name: `get database from ${url}` }),
    flatMap(response => fromEither(decode(response, DbRawType.decode))),
    map(raw => Object.entries(raw).map(([name, info]) => ({ name, info }))),
  );
};

type DbRecordWithSummary = DbRecord & { summary: string };
const addSummary = (entry: DbRecord): TaskEither<AppError, DbRecordWithSummary> => {
  console.log(`summarize ${entry.name}`);
  const llm = new ChatOpenAI({ modelName: 'gpt-4-1106-preview', temperature: 0, maxTokens: 4096 });
  const prompt = ChatPromptTemplate.fromMessages([
    ['system', SummarySystemMessage],
    ['human', '{info}'],
  ]);
  const longChain = new LLMChain({ llm, prompt });

  return pipe(
    tryCatch(() => longChain.call({ info: entry.info.join('\n') }), 'summary'),
    map(result => result.text),
    map(summary => ({ ...entry, summary })),
  );
};

const SummarySystemMessage = `
Based on the user-provided fragment, it is your responsibility as a researcher to create a summary note. 
You pay special attention to remembering the person's interests, favourite movies or books, skills, and pets.

Rules:
- filter out repetitive information.
- always speak English,
- use very concise sentences, without bullet points.
- do not use the person name
- do not add your own comments
`;

const combineIntoContextString = (db: readonly DbRecordWithSummary[]): string =>
  db.map(({ name, summary }) => `####\n${name}:\n${summary}`).join('\n\n');
