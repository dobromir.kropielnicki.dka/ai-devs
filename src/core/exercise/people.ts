import { flatMap, fromEither, left, map, of, TaskEither } from 'fp-ts/TaskEither';
import { AppError, appError, Exercise } from '@core/types.ts';
import { pipe } from 'fp-ts/function';
import * as t from 'io-ts';
import { v4 as uuidv4 } from 'uuid';
import { ChatOpenAI } from 'langchain/chat_models/openai';
import {
  batchUpsert,
  CollectionEntry,
  CollectionUpsertResult,
  recreateCollection,
  searchCollection,
} from '@infrastructure/qdrant';
import {
  embeddingQuery,
  enhanceWithEmbeddings,
  WithEmbedding,
} from '@infrastructure/openAi/embbeding.ts';
import { decode } from '@utils/decode.ts';
import { httpGet } from '@utils/httpRequest.ts';
import { tryCatch } from '@utils/fpHelpers.ts';
import { ChatPromptTemplate } from 'langchain/prompts';
import { LLMChain } from 'langchain/chains';

const VectorDbCollection = 'ai_devs--people';

export const people: Exercise<string> = {
  name: 'people',
  action: (task: unknown) =>
    pipe(
      getDefinition(task),
      flatMap(({ url, question }) =>
        pipe(
          recreateCollection(VectorDbCollection),
          flatMap(() => getKnowledge(url)),
          flatMap(knowledge => enhanceWithEmbeddings(knowledge, knowledgeAsText)),
          flatMap(buildVectorSearchIndex),
          flatMap(() => embeddingQuery(question)),
          flatMap(vector => searchCollection<People>(VectorDbCollection, vector, { limit: 5 })),
          map(result => result.map(item => item.payload)),
          flatMap(people => answerQuestion(question, asContext(people))),
          flatMap(answer =>
            answer === IDontKnow ? left(appError('answer not found')) : of(answer),
          ),
        ),
      ),
    ),
};

const getDefinition = (task: unknown): TaskEither<AppError, { question: string; url: string }> => {
  return pipe(
    fromEither(decode(task, t.type({ data: t.string, question: t.string }).decode)),
    map(({ data, question }) => ({ question, url: data })),
  );
};

const PeopleType = t.type({
  imie: t.string,
  nazwisko: t.string,
  wiek: t.number,
  o_mnie: t.string,
  ulubiona_postac_z_kapitana_bomby: t.string,
  ulubiony_serial: t.string,
  ulubiony_film: t.string,
  ulubiony_kolor: t.string,
});
type People = t.TypeOf<typeof PeopleType>;
type PieceOfKnowledge = People & { uuid: string };
const getKnowledge = (url: string): TaskEither<AppError, PieceOfKnowledge[]> => {
  console.log(`get knowledge from ${url}`);
  return pipe(
    httpGet(url, { name: `get knowledge from ${url}` }),
    flatMap(response => fromEither(decode(response, t.array(PeopleType).decode))),
    map(list => list.map(piece => ({ ...piece, uuid: uuidv4() }))),
  );
};

const knowledgeAsText = (item: PieceOfKnowledge): string => {
  return `Nazywam się ${item.imie} ${item.nazwisko}, ${item.o_mnie}`;
};

const buildVectorSearchIndex = (
  knowledge: WithEmbedding<PieceOfKnowledge>[],
): TaskEither<AppError, CollectionUpsertResult> => {
  console.log(`build search index: ${knowledge.length} items`);
  return batchUpsert(VectorDbCollection, knowledge.map(toVectorEntry));
};

const toVectorEntry = (item: WithEmbedding<PieceOfKnowledge>): CollectionEntry => {
  const { uuid: _, embedding: __, ...payload } = item;
  return {
    id: item.uuid,
    vector: item.embedding,
    payload,
  };
};

const answerQuestion = (question: string, context: string): TaskEither<AppError, string> => {
  console.log(`asking llm: ${question}`);

  const llm = new ChatOpenAI({ modelName: 'gpt-3.5-turbo' });
  const prompt = ChatPromptTemplate.fromMessages([
    ['system', systemPromptTmp(context)],
    ['human', '{question}'],
  ]);
  const longChain = new LLMChain({ llm, prompt });

  return pipe(
    tryCatch(() => longChain.call({ question }), 'answering question'),
    map(result => result.text),
  );
};

const asContext = (people: People[]) => {
  return people
    .map(
      person =>
        `osoba: ${person.imie} ${person.nazwisko},\nopis: ${person.o_mnie}\nulubiony kolor: ${person.ulubiony_kolor}`,
    )
    .join('\n\n');
};

const IDontKnow = 'nie wiem';
const systemPromptTmp = (context: string) => `
Odpowiedz zwięźle na podstawie dostarczonej w kontekście informacji. Odpowiedz "${IDontKnow}" jeśli brakuje informacji do udzielenia odpowiedzi.

Kontekst ###
${context}
###
`;
