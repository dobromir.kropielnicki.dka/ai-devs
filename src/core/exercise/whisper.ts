import * as t from 'io-ts';
import { pipe } from 'fp-ts/function';
import { either as E } from 'fp-ts';
import { filterOrElse, flatMap, fromEither, map, TaskEither } from 'fp-ts/TaskEither';

import { AppError, appError, Exercise } from '@core/types.ts';
import { decode } from '@utils/decode.ts';
import { isNil } from '@utils/nil.ts';
import { tryCatch } from '@utils/fpHelpers.ts';
import { openAiClient } from '@infrastructure/openAi/client.ts';
import { findUrl } from '@utils/regexp.ts';

export const whisper: Exercise<string> = {
  name: 'whisper',
  action: (task: unknown) => {
    return pipe(
      fromEither(getTaskMessage(task)),
      flatMap(message => fromEither(extractUrl(message))),
      flatMap(downloadMp3),
      flatMap(createTranscriptions),
    );
  },
};

const getTaskMessage = (task: unknown): E.Either<AppError, string> => {
  return pipe(
    decode(task, t.type({ msg: t.string }).decode),
    E.map(({ msg }) => msg),
  );
};

const extractUrl = (message: string): E.Either<AppError, string> => {
  const url = findUrl(message);
  return isNil(url) ? E.left(appError('url not found', message)) : E.right(url);
};

const downloadMp3 = (url: string): TaskEither<AppError, File> => {
  return pipe(
    tryCatch(() => fetch(url), `download mp3 ${url}`),
    filterOrElse(
      response => response.ok,
      response => appError('mp3 response is not ok', response),
    ),
    flatMap(response => tryCatch(() => response.blob(), 'reading audio blob')),
    map(blob => new File([blob], 'audio.mp3')),
  );
};

const createTranscriptions = (file: File): TaskEither<AppError, string> => {
  return pipe(
    tryCatch(
      () =>
        openAiClient.audio.transcriptions.create({
          model: 'whisper-1',
          file,
        }),
      'creating transcriptions',
    ),
    map(result => result.text),
  );
};
