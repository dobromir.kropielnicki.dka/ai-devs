import { AppError, appErrorTE, Exercise } from '@core/types.ts';
import { flatMap, fromEither, map, of, TaskEither } from 'fp-ts/TaskEither';
import { pipe } from 'fp-ts/function';
import { decode } from '@utils/decode.ts';
import * as t from 'io-ts';
import { HumanMessage, SystemMessage } from 'langchain/schema';
import {
  CountryPopulationSchema,
  tryCountryPopulation,
} from '@infrastructure/api/countries/countryPopulation.ts';
import {
  CurrentExchangeRateSchema,
  tryCurrentExchangeRate,
} from '@infrastructure/api/npb/currentExchangeRate.ts';
import {
  callForFunction,
  FunctionCallInfo,
  FunctionCallResult,
  isFunctionInfoCall,
} from '@infrastructure/openAi/functions.ts';

const Actions = [
  { scheme: CountryPopulationSchema, fn: tryCountryPopulation },
  { scheme: CurrentExchangeRateSchema, fn: tryCurrentExchangeRate },
];

export const knowledge: Exercise<string | number> = {
  name: 'knowledge',
  action: (task: unknown) =>
    pipe(
      getQuestion(task),
      flatMap(question => selectFunction(question)),
      flatMap(result => (isFunctionInfoCall(result) ? executeFunction(result) : of(result))),
    ),
};

const getQuestion = (task: unknown): TaskEither<AppError, string> =>
  pipe(
    fromEither(decode(task, t.type({ question: t.string }).decode)),
    map(({ question }) => question),
  );

const selectFunction = (question: string): TaskEither<AppError, FunctionCallResult> => {
  const messages = [
    new SystemMessage('Use the same language as the user.'),
    new HumanMessage(question),
  ];

  return callForFunction(
    messages,
    Actions.map(action => action.scheme),
  );
};

const executeFunction = (info: FunctionCallInfo): TaskEither<AppError, string | number> => {
  const { name, args } = info;
  console.log(`calling action: ${name} with params: ${JSON.stringify(args)}`);
  const action = Actions.find(action => action.scheme.name === name);
  return action ? action.fn(args) : appErrorTE('action not found', { name, args: args });
};
