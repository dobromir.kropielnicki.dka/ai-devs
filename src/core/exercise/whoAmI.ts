import { flatMap, fromEither, left, map, orElse, right, TaskEither } from 'fp-ts/TaskEither';
import { AppError, appError, Exercise } from '@core/types.ts';
import { ChatOpenAI } from 'langchain/chat_models/openai';
import { HumanMessage, SystemMessage } from 'langchain/schema';
import { pipe } from 'fp-ts/function';
import { log, tryCatch } from '@utils/fpHelpers.ts';
import { isNotEmpty } from '@utils/string.ts';
import { either as E } from 'fp-ts';
import * as t from 'io-ts';
import { decode } from '@utils/decode.ts';
import { getTaskByName } from '@infrastructure/aiDev/getTask.ts';

type TaskResponse = { answer: string; token: string };

export const whoAmI: Exercise<string> = {
  name: 'whoami',
  action: (task: unknown, token) => tryToGuess(task, [], token),
};

const tryToGuess = (
  task: unknown,
  hints: string[],
  token: string,
): TaskEither<AppError, TaskResponse> => {
  if (hints.length > 10) {
    return left(appError('too many hints', hints));
  }

  return pipe(
    extractHint(task),
    flatMap(hint => {
      const allHints = hints.concat(hint);
      console.log('all hints', allHints);
      return pipe(
        askAboutName(allHints),
        log('asked about name'),
        flatMap(name => verifyName(name, allHints)),
        log('verified name'),
        map(answer => ({ answer, token })),
        orElse(() => tryAgain(allHints)),
      );
    }),
  );
};

const tryAgain = (allHints: string[]): TaskEither<AppError, TaskResponse> =>
  pipe(
    getTaskByName(whoAmI.name),
    flatMap(({ task, token }) => tryToGuess(task, allHints, token)),
  );

const extractHint = (task: unknown): TaskEither<AppError, string> =>
  fromEither(
    pipe(
      decode(task, t.type({ hint: t.string }).decode),
      E.map(result => result.hint),
    ),
  );

const askAboutName = (hints: string[]): TaskEither<AppError, string> => {
  const chat = new ChatOpenAI({ modelName: 'gpt-3.5-turbo' });
  const messages = [new SystemMessage(askSystemPrompt), new HumanMessage(hints.join('\n'))];
  return pipe(
    tryCatch(() => chat.call(messages), 'asking question'),
    map(response => response.content as string),
    log('ask about name result'),
    flatMap(result =>
      result.startsWith(`I don't know`) ? left(appError('unrecognized', result)) : right(result),
    ),
  );
};

const askSystemPrompt = `
You will get a trivia items about a very famous person. 
Try to guess the name. 
Return only name, nothing else. 
When you don't know say "I don't know".`;

const verifyName = (name: string, hints: string[]): TaskEither<AppError, string> => {
  const chat = new ChatOpenAI({ modelName: 'gpt-3.5-turbo' });
  const messages = [
    new SystemMessage(verifySystemPrompt),
    new HumanMessage(`${name}\n\n${hints.join('\n')}`),
  ];
  return pipe(
    tryCatch(() => chat.call(messages), 'verify name'),
    map(result => isNotEmpty(result.content) && result.content.toUpperCase() === 'YES'),
    log('verify name result'),
    flatMap(check => (check ? right(name) : left(appError('verification failed', name)))),
  );
};

const verifySystemPrompt = `
Verify if the given name of the famous person is in line with the attached facts. Return only YES or NO, nothing else. `;
