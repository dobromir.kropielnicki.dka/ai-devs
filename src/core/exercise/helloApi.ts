import { pipe } from 'fp-ts/function';
import { fromEither } from 'fp-ts/TaskEither';
import * as t from 'io-ts';
import { map } from 'fp-ts/Either';
import { Exercise } from '@core/types.ts';
import { decode } from '@utils/decode.ts';

export const helloApi: Exercise<string> = {
  name: 'helloapi',
  action: (task: unknown) =>
    fromEither(
      pipe(
        decode(task, ResponseType.decode),
        map(result => result.cookie),
      ),
    ),
};

const ResponseType = t.type({ cookie: t.string });
