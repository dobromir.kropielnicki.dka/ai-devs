import { pipe } from 'fp-ts/function';
import { flatMap, fromEither, map, TaskEither } from 'fp-ts/TaskEither';
import * as t from 'io-ts';
import { AppError, Exercise } from '@core/types.ts';
import { decode } from '@utils/decode.ts';
import { httpPost } from '@utils/httpRequest.ts';

export const meme: Exercise<string> = {
  name: 'meme',
  action: (task: unknown) =>
    pipe(getMemeContent(task), map(toTemplateParams), flatMap(generateMemeUrl)),
};

const MemeType = t.type({ text: t.string, image: t.string });
type Meme = t.TypeOf<typeof MemeType>;
const getMemeContent = (task: unknown): TaskEither<AppError, Meme> =>
  pipe(
    fromEither(decode(task, t.type({ text: t.string, image: t.string }).decode)),
    map(({ text, image }) => ({ text, image })),
  );

type TemplateParams = {
  template: string;
  data: Record<string, string>;
};
const toTemplateParams = (meme: Meme): TemplateParams => ({
  template: 'good-giants-tickle-equally-1454',
  data: {
    'main_img.src': meme.image,
    'title.text': meme.text,
  },
});

const generateMemeUrl = (params: TemplateParams): TaskEither<AppError, string> => {
  const options = {
    name: 'meme',
    headers: {
      'Content-Type': 'application/json',
      'x-api-key': process.env.RENDERFORM_API_KEY ?? '',
    },
  };
  return pipe(
    httpPost('https://api.renderform.io/api/v2/render?output=json', params, options),
    flatMap(response => fromEither(decode(response, t.type({ href: t.string }).decode))),
    map(({ href }) => href),
  );
};
