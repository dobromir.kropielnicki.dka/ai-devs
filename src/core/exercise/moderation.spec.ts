import { describe, expect, test } from 'bun:test';
import { isLeft, matchW } from 'fp-ts/Either';
import { pipe } from 'fp-ts/function';
import { moderation } from '@exercise/moderation.ts';

describe('moderation', () => {
  const { name, action } = moderation;

  test('has a proper name', async () => {
    expect(name).toBe('moderation');
  });

  test('returns error when task can no be decoded', async () => {
    const result = await action('test', 'token')();
    expect(isLeft(result)).toBeTrue();
    expect(
      pipe(
        result,
        matchW(
          error => error.message,
          ok => ok,
        ),
      ),
    ).toEqual('unrecognized data');
  });
});
