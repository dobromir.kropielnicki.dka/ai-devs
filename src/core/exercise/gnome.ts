import { AppError, Exercise } from '@core/types.ts';
import { flatMap, map, TaskEither } from 'fp-ts/TaskEither';
import { pipe } from 'fp-ts/function';
import { parseTE } from '@utils/decode.ts';
import * as t from 'io-ts';
import { OpenAI } from 'openai';
import { openAiChatCall, systemMessage, userMessage } from '@infrastructure/openAi/chat.ts';

export const gnome: Exercise<string> = {
  name: 'gnome',
  action: (task: unknown) =>
    pipe(getImageUrl(task), flatMap(askVisionAboutGnomeHat), map(normalizeAnswer)),
};

const getImageUrl = (task: unknown): TaskEither<AppError, string> =>
  pipe(
    parseTE(task, t.type({ url: t.string }).decode),
    map(({ url }) => url),
  );

const ErrorCode = 'ERROR';
const askVisionAboutGnomeHat = (url: string): TaskEither<AppError, string> => {
  console.log(`asking vision about image: ${url}`);
  const params: OpenAI.Chat.ChatCompletionCreateParams = {
    model: 'gpt-4-vision-preview',
    max_tokens: 1000,
    messages: [
      systemMessage(
        `Opiszujesz wyłacznie kolor czapki krasnala. Opowiadasz nazwą koloru lub słowem ${ErrorCode} w każdym innym przypadku`,
      ),
      userMessage([{ type: 'image_url', image_url: { url } }]),
    ],
  };
  return openAiChatCall(params, 'asking vision about image');
};

const normalizeAnswer = (answer: string): string => {
  return answer !== ErrorCode ? answer.toLowerCase() : ErrorCode;
};
