import * as t from 'io-ts';
import { either as E } from 'fp-ts';
import { flatMap, fromEither, TaskEither } from 'fp-ts/TaskEither';
import { AppError, Exercise } from '@core/types.ts';
import { pipe } from 'fp-ts/function';
import { HumanMessage, SystemMessage } from 'langchain/schema';

import { longChainCall } from '@infrastructure/openAi/chat.ts';
import { decode } from '@utils/decode.ts';

export const inPrompt: Exercise<string> = {
  name: 'inprompt',
  action: (task: unknown) => {
    return pipe(
      fromEither(getTaskDefinition(task)),
      flatMap(({ input, question }) => {
        const knowledge = input.map(toKnowledgeDocument);
        const context = findContext(knowledge, question);
        return askQuestion(question, context);
      }),
    );
  },
};

const askQuestion = (
  question: string,
  context: KnowledgeDocument[],
): TaskEither<AppError, string> => {
  const queryContext = context.map(document => document.content).join('\n');
  return longChainCall(
    [
      new SystemMessage(
        `Odpowiedz na pytanie używając tylko poniższych informacji.\n\n###\n${queryContext}\n###`,
      ),
      new HumanMessage(question),
    ],
    'asking question',
  );
};

const findContext = (knowledge: KnowledgeDocument[], question: string): KnowledgeDocument[] => {
  const questionWords = question.replace('?', '').split(' ');
  return knowledge.filter(document => questionWords.includes(document.name));
};

const TaskDefinitionType = t.type({ input: t.array(t.string), question: t.string });
type TaskDefinition = t.TypeOf<typeof TaskDefinitionType>;
const getTaskDefinition = (task: unknown): E.Either<AppError, TaskDefinition> => {
  return decode(task, TaskDefinitionType.decode);
};

type KnowledgeDocument = { name: string; content: string };
const toKnowledgeDocument = (value: string): KnowledgeDocument => {
  return {
    name: takeFirstWord(value),
    content: value,
  };
};

const takeFirstWord = (value: string): string => value.split(' ', 1)[0] ?? '';
