import { flatMap, fromEither, left, map, orElse, TaskEither } from 'fp-ts/TaskEither';
import { AppError, appError, Exercise } from '@core/types.ts';
import * as t from 'io-ts';
import { either as E } from 'fp-ts';
import { decode } from '@utils/decode.ts';
import { pipe } from 'fp-ts/function';
import { log, tryCatch } from '@utils/fpHelpers.ts';
import { HumanMessage, SystemMessage } from 'langchain/schema';
import { ChatOpenAI } from 'langchain/chat_models/openai';

export const scraper: Exercise<string> = {
  name: 'scraper',
  action: (task: unknown) =>
    pipe(
      fromEither(readTask(task)),
      flatMap(({ url, question }) =>
        pipe(
          tryReadPage(url, 10),
          log(url),
          flatMap(page => answerQuestion(question, page)),
        ),
      ),
    ),
};

const readTask = (task: unknown): E.Either<AppError, { question: string; url: string }> => {
  return pipe(
    decode(task, t.type({ input: t.string, question: t.string }).decode),
    E.map(({ input, question }) => ({ question, url: input })),
  );
};

const tryReadPage = (url: string, attempt: number): TaskEither<AppError, string> => {
  if (attempt <= 0) {
    return left(appError('too many attempts'));
  }
  return pipe(
    fetchPage(url),
    flatMap(parsePage),
    orElse(() => tryReadPage(url, attempt - 1)),
  );
};

const fetchPage = (url: string): TaskEither<AppError, Response> => {
  return tryCatch(
    () =>
      fetch(url, {
        headers: {
          'User-Agent':
            'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36',
        },
      }),
    'fetching page',
  );
};

const parsePage = (response: Response): TaskEither<AppError, string> => {
  return response.ok
    ? tryCatch(() => response.text(), 'reading page')
    : left(appError('response not ok', response));
};

const answerQuestion = (question: string, context: string): TaskEither<AppError, string> => {
  const chat = new ChatOpenAI({ modelName: 'gpt-3.5-turbo', maxTokens: 1000 });
  const messages = [new SystemMessage(systemPromptTmp(context)), new HumanMessage(question)];
  return pipe(
    tryCatch(() => chat.call(messages), 'answering question'),
    map(result => result.content),
  );
};

const systemPromptTmp = (context: string) => `
Answer the question using only the information from the provided context.

Rules:
- the answer should be in Polish.
- be very concise.
- return only the answer, nothing else.

Context:
###
${context}
###
`;
