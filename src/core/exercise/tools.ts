import { flatMap, fromEither, map, of, TaskEither } from 'fp-ts/TaskEither';
import { pipe } from 'fp-ts/function';
import * as t from 'io-ts';
import { HumanMessage, SystemMessage } from 'langchain/schema';
import { OpenAI } from 'openai';
import { AppError, appErrorTE, Exercise } from '@core/types.ts';
import { decode } from '@utils/decode.ts';
import {
  callForFunction,
  FunctionCallInfo,
  isFunctionInfoCall,
} from '@infrastructure/openAi/functions.ts';

type ToolInfo = { tool: 'ToDo' | 'Calendar'; desc: string; date?: string };
export const tools: Exercise<ToolInfo> = {
  name: 'tools',
  action: (task: unknown) => pipe(getQuestion(task), flatMap(selectAction), flatMap(getToolInfo)),
};

const getQuestion = (task: unknown): TaskEither<AppError, string> =>
  pipe(
    fromEither(decode(task, t.type({ question: t.string }).decode)),
    map(({ question }) => question),
  );

const selectAction = (question: string): TaskEither<AppError, FunctionCallInfo> => {
  const messages = [
    new SystemMessage(`Current data: ${new Date().toISOString().split('T')[0]}`),
    new HumanMessage(question),
  ];

  return pipe(
    callForFunction(messages, [AddToDoSchema, AddToCalendarSchema]),
    flatMap(result =>
      isFunctionInfoCall(result)
        ? of(result)
        : appErrorTE<FunctionCallInfo>('direct answer', result),
    ),
  );
};

const getToolInfo = ({ name, args }: FunctionCallInfo): TaskEither<AppError, ToolInfo> => {
  switch (name) {
    case AddToDoSchema.name:
      return pipe(
        fromEither(decode(args, AddToDoParamsType.decode, 'unrecognized addToDo params')),
        map(({ task }) => ({ tool: 'ToDo', desc: task })),
      );
    case AddToCalendarSchema.name:
      return pipe(
        fromEither(
          decode(args, AddToCalendarParamsType.decode, 'unrecognized addToCalendar params'),
        ),
        map(({ event, date }) => ({ tool: 'Calendar', desc: event, date })),
      );
    default:
      return appErrorTE('unknown action', { name, args });
  }
};

export const AddToDoSchema: OpenAI.FunctionDefinition = {
  name: 'addToDo',
  description: 'Adds a new task to the list of tasks.',
  parameters: {
    type: 'object',
    properties: {
      task: {
        type: 'string',
        description: 'The task to be added to the list of tasks.',
      },
    },
    required: ['task'],
  },
};
const AddToDoParamsType = t.type({ task: t.string });

export const AddToCalendarSchema: OpenAI.FunctionDefinition = {
  name: 'addToCalendar',
  description: 'Adds a new event to the calendar.',
  parameters: {
    type: 'object',
    properties: {
      event: {
        type: 'string',
        description: 'The event to be added to the calendar.',
      },
      date: {
        type: 'string',
        description: 'The date of the event formatted as YYYY-MM-DD',
      },
    },
    required: ['event', 'date'],
  },
};
const AddToCalendarParamsType = t.type({ event: t.string, date: t.string });
