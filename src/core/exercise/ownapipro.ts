import { AppError, Exercise } from '@core/types.ts';
import { map, of, TaskEither } from 'fp-ts/TaskEither';
import { pipe } from 'fp-ts/function';
import { log, tryCatch } from '@utils/fpHelpers.ts';
import { ChatOpenAI } from 'langchain/chat_models/openai';
import { ChatPromptTemplate, MessagesPlaceholder } from 'langchain/prompts';
import { ConversationChain } from 'langchain/chains';
import { BufferMemory } from 'langchain/memory';

const apiEndpoint = `${process.env.DK_API_URL}/${process.env.DK_API_SECRET}/ownapipro`;
export const ownApiPro: Exercise<string> = {
  name: 'ownapipro',
  action: () => of(apiEndpoint),
};

const llm = new ChatOpenAI({ modelName: 'gpt-3.5-turbo' });
const prompt = ChatPromptTemplate.fromMessages([
  ['system', 'Answer concisely in the same language in which the user asks the question.'],
  new MessagesPlaceholder('history'),
  ['user', '{question}'],
]);
const chain = new ConversationChain({
  memory: new BufferMemory({ returnMessages: true, memoryKey: 'history' }),
  prompt,
  llm,
});

export const ownApiProUseCase = (question: string): TaskEither<AppError, string> => {
  console.log('asking llm:', question);

  return pipe(
    tryCatch(() => chain.call({ question }), 'llm call'),
    map(result => result.response),
    log('llm reply'),
  );
};
