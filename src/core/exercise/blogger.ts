import { AppError, Exercise } from '@core/types.ts';
import { ChatOpenAI } from 'langchain/chat_models/openai';
import { ChatPromptTemplate, MessagesPlaceholder } from 'langchain/prompts';
import { ConversationChain } from 'langchain/chains';
import { BufferMemory } from 'langchain/memory';
import { either as E, taskEither as TE } from 'fp-ts';
import { pipe } from 'fp-ts/function';
import { decode } from '@utils/decode.ts';
import * as t from 'io-ts';
import { tryCatch } from '@utils/fpHelpers.ts';

export const blogger: Exercise<string[]> = {
  name: 'blogger',
  action: (task: unknown) => {
    const conversationChain = setupConversation();
    return pipe(
      TE.fromEither(getChapters(task)),
      TE.flatMap(chapters => TE.sequenceArray(postQuestions(chapters, conversationChain))),
      TE.map(results => results.map(result => result.response)),
    );
  },
};

const postQuestions = (questions: string[], conversation: ConversationChain) => {
  return questions.map(input => tryCatch(() => conversation.call({ input }), 'chain call'));
};

const getChapters = (task: unknown): E.Either<AppError, string[]> => {
  const ResponseType = t.type({ blog: t.array(t.string) });
  return pipe(
    decode(task, ResponseType.decode),
    E.map(result => result.blog),
  );
};

const setupConversation = (): ConversationChain => {
  const chat = new ChatOpenAI({
    temperature: 0,
    maxTokens: 500,
    frequencyPenalty: 0.4,
    modelName: 'gpt-3.5-turbo',
  });

  const chatPrompt = ChatPromptTemplate.fromMessages([
    ['system', systemMessage],
    new MessagesPlaceholder('history'),
    ['human', '{input}'],
  ]);

  return new ConversationChain({
    memory: new BufferMemory({ returnMessages: true, memoryKey: 'history' }),
    prompt: chatPrompt,
    llm: chat,
  });
};

const systemMessage = `
Prowadzisz blog kulinarny specjalizujący się w kuchni włoskiej.  

Poproszę Cię o przygotowanie wpisu na blog. 
Będę Ci podawał tytuły rozdziałów jako kolejne wiadomości.
Porozmawiamy o pizzy. 

Twoje odpowiedzi zawsze są
- zwięźle,
- rzeczowe,
- zawierające komplet informacji
`;
