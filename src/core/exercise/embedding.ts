import * as t from 'io-ts';
import { flatMap, fromEither, map, TaskEither } from 'fp-ts/TaskEither';
import { either as E } from 'fp-ts';
import { pipe } from 'fp-ts/function';
import { HumanMessage, SystemMessage } from 'langchain/schema';

import { AppError, Exercise } from '@core/types.ts';
import { openAiClient } from '@infrastructure/openAi/client.ts';
import { longChainCall } from '@infrastructure/openAi/chat.ts';
import { tryCatch } from '@utils/fpHelpers.ts';
import { decode } from '@utils/decode.ts';

export const embedding: Exercise<number[]> = {
  name: 'embedding',
  action: (task: unknown) =>
    pipe(
      fromEither(getTaskDefinition(task)),
      flatMap(({ msg }) => findTextToConvert(msg)),
      flatMap(createEmbedding),
    ),
};

const TaskDefinitionType = t.type({ msg: t.string });
type TaskDefinition = t.TypeOf<typeof TaskDefinitionType>;
const getTaskDefinition = (task: unknown): E.Either<AppError, TaskDefinition> => {
  return decode(task, TaskDefinitionType.decode);
};

const findTextToConvert = (message: string): TaskEither<AppError, string> => {
  return longChainCall(
    [
      new SystemMessage(
        `Extract part of the sentence for which action is required. Return only the extracted part, nothing else.`,
      ),
      new HumanMessage(message),
    ],
    'finding text to convert',
  );
};

const createEmbedding = (input: string): TaskEither<AppError, number[]> =>
  pipe(
    tryCatch(
      () =>
        openAiClient.embeddings.create({
          model: 'text-embedding-ada-002',
          input,
          encoding_format: 'float',
        }),
      'creating embedding',
    ),
    map(result => result.data[0]?.embedding ?? []),
  );
