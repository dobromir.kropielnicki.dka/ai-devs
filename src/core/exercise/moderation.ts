import * as t from 'io-ts';
import { taskEither as TE, either as E } from 'fp-ts';
import { AppError, Exercise } from '@core/types.ts';
import { pipe } from 'fp-ts/function';
import { decode } from '@utils/decode.ts';
import { moderate } from '@infrastructure/openAi/moderate.ts';

export const moderation: Exercise<number[]> = {
  name: 'moderation',
  action: (task: unknown) => {
    return pipe(
      TE.fromEither(getSentences(task)),
      TE.flatMap(sentences => TE.sequenceArray(sentences.map(moderate))),
      TE.map(results => results.map(result => (result ? 1 : 0))),
    );
  },
};

const getSentences = (task: unknown): E.Either<AppError, string[]> => {
  const ResponseType = t.type({ input: t.array(t.string) });
  return pipe(
    decode(task, ResponseType.decode),
    E.map(result => result.input),
  );
};
