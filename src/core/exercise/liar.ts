import * as t from 'io-ts';
import { pipe } from 'fp-ts/function';
import { flatMap, map, TaskEither } from 'fp-ts/TaskEither';
import { HumanMessage, SystemMessage } from 'langchain/schema';

import { config } from '../../config.ts';
import { AppError, Exercise } from '@core/types.ts';
import { longChainCall } from '@infrastructure/openAi/chat.ts';
import { postTaskData } from '@infrastructure/aiDev/postTask.ts';
import { log } from '@utils/fpHelpers.ts';

export const liar: Exercise<string> = {
  name: 'liar',
  action: (_, token: string) =>
    pipe(
      generateQuestion(),
      log('question'),
      flatMap(question =>
        pipe(
          getAnswer(question, token),
          log('answer'),
          flatMap(answer => verifyAnswer(question, answer)),
        ),
      ),
      log('verification'),
      map(result => (result ? 'YES' : 'NO')),
    ),
};

const generateQuestion = (): TaskEither<AppError, string> => {
  return longChainCall([new HumanMessage(questionPrompt)], 'generate question');
};

const getAnswer = (question: string, token: string): TaskEither<AppError, string> =>
  pipe(
    postTaskData(
      { question },
      {
        token,
        validator: t.type({ answer: t.string }).decode,
        formData: true,
      },
    ),
    map(result => result.answer),
  );

const verifyAnswer = (question: string, answer: string): TaskEither<AppError, boolean> => {
  return pipe(
    longChainCall(
      [
        new SystemMessage(factCheckerSystemPrompt),
        new HumanMessage(JSON.stringify({ question, answer })),
      ],
      'generate question',
    ),
    map(result => result === 'true'),
  );
};

const questionPrompt = `Generate a simple, factual question pertaining to geography or history, for which the answer is easy to check. Only generate one question, and ensure that it requires specific knowledge to answer. For example, the question could be about the capital of a country, the largest lake in a particular region, or who held a specific historical title during a certain year.`;
const factCheckerSystemPrompt = `
An AI assistant is programmed to verify the factual accuracy of responses given to single questions. Given the question and answer, the assistant's task is to either affirm the correctness of the answer ("true"), deny it ("false"), or state that the verification result is indeterminate ("false"). Please match these responses based on the factual accuracy of the user's input."

For instance, when provided with the following instructions:
User: { "question": "What color is the sky?", "answer": "Blue" }
Assistant: "true"

User: { "question": "What is the biggest lake in the US?" "answer": "Victoria Lake".
Assistant: "false"

User:  {"question": "Which colour do you like best?", "answer": "pink"}
Assistant: "false"

The assistant's answers should always be one of the possible responses - "true", "false" - depending upon the verifiable truth of the answer provided in the scenario. It should be noted that the assistant does not provide explanations or engage in discourse beyond a simple affirmation or denial of the stated facts.`;
