import { Exercise } from '@core/types.ts';
import { OpenAI } from 'openai';
import { of } from 'fp-ts/TaskEither';

export const functions: Exercise<unknown> = {
  name: 'functions',
  action: () => of(schema),
};

const schema: OpenAI.Chat.ChatCompletionCreateParams.Function = {
  name: 'addUser',
  description: 'Add user to database',
  parameters: {
    type: 'object',
    properties: {
      name: {
        type: 'string',
        description: 'Nam of the user',
      },
      surname: {
        type: 'string',
        description: 'Surname of the user',
      },
      year: {
        type: 'number',
        description: 'Year of birth of the user',
      },
    },
    required: ['name', 'surname', 'year'],
  },
};
