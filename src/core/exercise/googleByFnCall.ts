import { flatMap, fromOption, map, of, TaskEither } from 'fp-ts/TaskEither';
import { appError, AppError, appErrorTE, Exercise } from '@core/types.ts';
import { ChatOpenAI } from 'langchain/chat_models/openai';
import { ChatPromptTemplate } from 'langchain/prompts';
import { LLMChain } from 'langchain/chains';
import { pipe } from 'fp-ts/function';
import { tryCatch } from '@utils/fpHelpers.ts';
import { parseJson, parseTE } from '@utils/decode.ts';
import * as t from 'io-ts';
import { HumanMessage, SystemMessage } from 'langchain/schema';
import { fromNullable } from 'fp-ts/Option';
import { googleIt, GoogleResult } from '@infrastructure/serpiapi';
import { OpenAI } from 'openai';
import {
  callForFunction,
  FunctionCallInfo,
  isFunctionInfoCall,
} from '@infrastructure/openAi/functions.ts';

const apiEndpoint = `${process.env.DK_API_URL}/${process.env.DK_API_SECRET}/google`;

export const googleByFnCall: Exercise<string> = {
  name: 'google',
  action: () => of(apiEndpoint),
};

export const googleUseCaseFnCall = (question: string): TaskEither<AppError, string> => {
  return pipe(createGoogleQueryFnCall(question), flatMap(googleIt), flatMap(takeFirstLink));
};

const takeFirstLink = (result: GoogleResult): TaskEither<AppError, string> => {
  return fromOption(() => appError('link not found'))(
    fromNullable(result.organic_results[0]?.link),
  );
};

const createGoogleQueryFnCall = (question: string): TaskEither<AppError, string> => {
  console.log(`creating query for: ${question}`);
  return pipe(
    callForFunction([new HumanMessage(question)], [SearchInGoogleSchema]),
    flatMap(result =>
      isFunctionInfoCall(result)
        ? of(result)
        : appErrorTE<FunctionCallInfo>('direct answer', result),
    ),
    flatMap(({ args }) => parseTE(args, SearchInGoogleParam.decode, 'unrecognized params')),
    map(({ query }) => query),
  );
};

const SearchInGoogleSchema: OpenAI.FunctionDefinition = {
  name: 'searchInGoogle',
  description: 'Search information in google',
  parameters: {
    type: 'object',
    properties: {
      query: {
        type: 'string',
        description: 'Query which will be used for googling to get the best answer',
      },
    },
    required: ['query'],
  },
};
const SearchInGoogleParam = t.type({ query: t.string });
