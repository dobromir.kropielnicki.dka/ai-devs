import { AppError, Exercise } from '@core/types.ts';
import { map, of, TaskEither } from 'fp-ts/TaskEither';
import { pipe } from 'fp-ts/function';
import { log, tryCatch } from '@utils/fpHelpers.ts';
import { ChatOpenAI } from 'langchain/chat_models/openai';
import { ChatPromptTemplate } from 'langchain/prompts';
import { LLMChain } from 'langchain/chains';

const apiEndpoint = `${process.env.DK_API_URL}/${process.env.DK_API_SECRET}/md2html`;
export const md2html: Exercise<string> = {
  name: 'md2html',
  action: () => of(apiEndpoint),
};

export const md2htmlUseCase = (markdown: string): TaskEither<AppError, string> => {
  console.log('markdown:', markdown);
  const llm = new ChatOpenAI({ modelName: 'ft:gpt-3.5-turbo-1106:dunning-kruger-associates::8OTDe90j' });
  const prompt = ChatPromptTemplate.fromMessages([
    ['system', 'ai-dev_md2html'],
    ['user', '{markdown}'],
  ]);
  const longChain = new LLMChain({ llm, prompt });

  return pipe(
    tryCatch(() => longChain.call({ markdown }), 'llm call'),
    map(({ text }) => text),
    log('llm reply'),
  );
};
