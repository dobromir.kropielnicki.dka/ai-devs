import { describe, expect, test } from 'bun:test';
import { helloApi } from './helloApi.ts';
import { isLeft, isRight, matchW } from 'fp-ts/Either';
import { pipe } from 'fp-ts/function';

describe('helloApi', () => {
  const { name, action } = helloApi;

  test('has a proper name', async () => {
    expect(name).toBe('helloapi');
  });

  test('returns error when task can no be decoded', async () => {
    const result = await action('test', 'token')();
    expect(isLeft(result)).toBeTrue();
    expect(
      pipe(
        result,
        matchW(
          error => error.message,
          ok => ok,
        ),
      ),
    ).toEqual('unrecognized data');
  });

  test('returns cookie properties', async () => {
    const cookie = 'abc';
    const result = await action({ cookie }, 'token')();
    expect(isRight(result)).toBeTrue();
    expect(
      pipe(
        result,
        matchW(
          error => error,
          ok => ok,
        ),
      ),
    ).toEqual(cookie);
  });
});
