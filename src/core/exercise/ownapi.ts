import { AppError, Exercise } from '@core/types.ts';
import { map, of, TaskEither } from 'fp-ts/TaskEither';
import { pipe } from 'fp-ts/function';
import { log, tryCatch } from '@utils/fpHelpers.ts';
import { ChatOpenAI } from 'langchain/chat_models/openai';
import { ChatPromptTemplate } from 'langchain/prompts';
import { LLMChain } from 'langchain/chains';

const apiEndpoint = `${process.env.DK_API_URL}/${process.env.DK_API_SECRET}/ownapi`;
export const ownApi: Exercise<string> = {
  name: 'ownapi',
  action: () => of(apiEndpoint),
};

export const ownApiUseCase = (question: string): TaskEither<AppError, string> => {
  console.log('asking llm:', question);
  const llm = new ChatOpenAI({ modelName: 'gpt-3.5-turbo' });
  const prompt = ChatPromptTemplate.fromMessages([
    ['system', 'Answer concisely in the same language in which the user asks the question.'],
    ['user', '{question}'],
  ]);
  const longChain = new LLMChain({ llm, prompt });

  return pipe(
    tryCatch(() => longChain.call({ question }), 'llm call'),
    map(({ text }) => text),
    log('llm reply'),
  );
};
