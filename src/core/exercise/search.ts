import { flatMap, fromEither, fromOption, left, map, of, TaskEither } from 'fp-ts/TaskEither';
import * as t from 'io-ts';
import { pipe } from 'fp-ts/function';
import { v4 as uuidv4 } from 'uuid';
import { AppError, appError, Exercise } from '@core/types.ts';
import {
  batchUpsert,
  CollectionEntry,
  CollectionUpsertResult,
  recreateCollection,
  searchCollection,
  SearchResult,
} from '@infrastructure/qdrant';
import { findUrl } from '@utils/regexp.ts';
import { decode } from '@utils/decode.ts';
import { isNotEmpty } from '@utils/string.ts';
import { httpGet } from '@utils/httpRequest.ts';
import { fromNullable } from 'fp-ts/Option';
import {
  embeddingQuery,
  enhanceWithEmbeddings,
  WithEmbedding,
} from '@infrastructure/openAi/embbeding.ts';

const VectorDbCollection = 'ai_devs--search';

export const search: Exercise<string> = {
  name: 'search',
  action: (task: unknown) =>
    pipe(
      getDefinition(task),
      flatMap(({ question, url }) =>
        pipe(
          recreateCollection(VectorDbCollection),
          flatMap(() => getKnowledge(url, 300)),
          flatMap(knowledge => enhanceWithEmbeddings(knowledge, knowledgeAsText)),
          flatMap(knowledge => buildVectorSearchIndex(VectorDbCollection, knowledge)),
          flatMap(() => embeddingQuery(question)),
          flatMap(vector =>
            searchCollection<{ url: string }>(VectorDbCollection, vector, { limit: 1 }),
          ),
          flatMap(selectAnswer),
        ),
      ),
    ),
};

const getDefinition = (task: unknown): TaskEither<AppError, { question: string; url: string }> => {
  return pipe(
    fromEither(decode(task, t.type({ msg: t.string, question: t.string }).decode)),
    map(({ msg, question }) => ({ question, url: findUrl(msg) })),
    flatMap(({ url, question }) =>
      isNotEmpty(url) ? of({ url, question }) : left(appError('url not found')),
    ),
  );
};

const PieceOfKnowledgeType = t.type({ title: t.string, url: t.string, info: t.string });
type PieceOfKnowledge = t.TypeOf<typeof PieceOfKnowledgeType> & { uuid: string };
const getKnowledge = (url: string, amount: number): TaskEither<AppError, PieceOfKnowledge[]> => {
  console.log(`get knowledge from ${url}`);
  return pipe(
    httpGet(url, { name: `get knowledge from ${url}` }),
    flatMap(response => fromEither(decode(response, t.array(PieceOfKnowledgeType).decode))),
    map(list => list.toSpliced(amount).map(piece => ({ ...piece, uuid: uuidv4() }))),
  );
};

const knowledgeAsText = (item: PieceOfKnowledge): string => {
  return `${item.title}\n\n ${item.info.replace('INFO:', '')}`;
};

const buildVectorSearchIndex = (
  collectionName: string,
  knowledge: WithEmbedding<PieceOfKnowledge>[],
): TaskEither<AppError, CollectionUpsertResult> => {
  return batchUpsert(collectionName, knowledge.map(toVectorEntry));
};

const toVectorEntry = (item: WithEmbedding<PieceOfKnowledge>): CollectionEntry => {
  const { uuid, embedding, title, info, url } = item;
  return {
    id: uuid,
    vector: embedding,
    payload: { title, info, url },
  };
};

const selectAnswer = (results: SearchResult<{ url: string }>[]): TaskEither<AppError, string> => {
  return pipe(
    fromOption(() => appError('Not found'))(fromNullable(results[0])),
    map(result => result.payload.url),
  );
};
