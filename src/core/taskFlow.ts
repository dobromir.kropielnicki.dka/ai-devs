import { pipe } from 'fp-ts/function';
import { flatMap, map } from 'fp-ts/TaskEither';

import { AnswerWithTokens, Exercise } from '@core/types.ts';
import { getTaskByName } from '@infrastructure/aiDev/getTask.ts';
import { sendAnswer } from '@infrastructure/aiDev/sendAnswer.ts';
import { log } from '@utils/fpHelpers.ts';

export const taskFlow = <T>({ name, action }: Exercise<T>) => {
  console.log(`running task: 🚀${name}`);
  return pipe(
    getTaskByName(name),
    log('get-task'),
    flatMap(({ task, token }) =>
      pipe(
        action(task, token),
        log('result'),
        map(result => (AnswerWithTokens.is(result) ? result : { answer: result, token })),
        flatMap(result => sendAnswer(result.answer, result.token)),
      ),
    ),
  );
};
