import { map, TaskEither } from 'fp-ts/TaskEither';
import { AppError } from '@core/types.ts';
import { execute } from '@utils/fpHelpers.ts';
import { pipe } from 'fp-ts/function';

export const toReply = (useCase: TaskEither<AppError, string>) =>
  execute(
    pipe(
      useCase,
      map(answer => ({ reply: answer })),
    ),
  );
