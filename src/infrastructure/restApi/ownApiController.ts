import { Elysia, t } from 'elysia';
import { ownApiUseCase } from '@exercise/ownapi.ts';
import { ownApiProUseCase } from '@exercise/ownapipro.ts';
import { toReply } from '@infrastructure/restApi/helper.ts';

export const ownApiController = (app: Elysia) =>
  app
    .guard({
      body: t.Object({ question: t.String() }),
    })
    .post('/ownapipro', ({ body }) => toReply(ownApiProUseCase(body.question)))
    .post('/ownapi', ({ body }) => toReply(ownApiUseCase(body.question)));
