import { Elysia, t } from 'elysia';
import { googleUseCase } from '@exercise/google.ts';
import { toReply } from '@infrastructure/restApi/helper.ts';

export const googleController = (app: Elysia) =>
  app
    .guard({
      body: t.Object({ question: t.String() }),
    })
    .post('/google', ({ body }) => toReply(googleUseCase(body.question)));
