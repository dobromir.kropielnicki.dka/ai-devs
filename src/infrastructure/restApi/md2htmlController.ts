import { Elysia, t } from 'elysia';
import { googleUseCase } from '@exercise/google.ts';
import { toReply } from '@infrastructure/restApi/helper.ts';
import { md2htmlUseCase } from '@exercise/md2html.ts';

export const md2htmlController = (app: Elysia) =>
  app
    .guard({
      body: t.Object({ question: t.String() }),
    })
    .post('/md2html', ({ body }) => toReply(md2htmlUseCase(body.question)));
