import * as t from 'io-ts';
import { flatMap, fromEither, fromOption, TaskEither } from 'fp-ts/TaskEither';
import { appError, AppError } from '@core/types.ts';
import { httpGet } from '@utils/httpRequest.ts';
import { pipe } from 'fp-ts/function';
import { decode } from '@utils/decode.ts';
import { fromNullable } from 'fp-ts/Option';
import { OpenAI } from 'openai';

/**
 * Provides information about the current exchange rate for a given currency.
 * @param currency - currency code in ISO 4217 format
 */
export const currentExchangeRate = (currency: string): TaskEither<AppError, number> =>
  pipe(
    httpGet(`http://api.nbp.pl/api/exchangerates/rates/A/${currency}/?format=json`, {
      name: 'nbp api exchange rates',
    }),
    flatMap(response => fromEither(decode(response, ResponseType.decode))),
    flatMap(({ rates }) =>
      fromOption(() => appError('rate not found'))(fromNullable(rates[0]?.mid)),
    ),
  );

const ResponseType = t.type({
  rates: t.array(
    t.type({
      mid: t.number,
    }),
  ),
});

export const tryCurrentExchangeRate = (params: unknown): TaskEither<AppError, number> => {
  const ParamsType = t.type({ currency: t.string });
  return pipe(
    fromEither(decode(params, ParamsType.decode, 'unrecognized currentExchangeRate params')),
    flatMap(({ currency }) => currentExchangeRate(currency)),
  );
};


export const CurrentExchangeRateSchema: OpenAI.FunctionDefinition = {
  name: 'currentExchangeRate',
  description: `Provides information about the current exchange rate for a given currency.`,
  parameters: {
    type: 'object',
    properties: {
      currency: {
        type: 'string',
        description: 'currency code in ISO 4217 format',
      },
    },
    required: ['currency'],
  },
};
