import * as t from 'io-ts';
import { flatMap, fromEither, fromOption, TaskEither } from 'fp-ts/TaskEither';
import { appError, AppError } from '@core/types.ts';
import { httpGet } from '@utils/httpRequest.ts';
import { pipe } from 'fp-ts/function';
import { decode } from '@utils/decode.ts';
import { fromNullable } from 'fp-ts/Option';
import { OpenAI } from 'openai';

/**
 * Provides information about the country's population.
 * @param country - country code in ISO 3166-1 alpha-3 format
 */
export const countryPopulation = (country: string): TaskEither<AppError, number> =>
  pipe(
    httpGet(`https://restcountries.com/v3.1/alpha/${country}`, {
      name: 'country api population',
    }),
    flatMap(response => fromEither(decode(response, ResponseType.decode))),
    flatMap(result =>
      fromOption(() => appError('rate not found'))(fromNullable(result[0]?.population)),
    ),
  );

const CountryType = t.type({ population: t.number });
const ResponseType = t.array(CountryType);

export const tryCountryPopulation = (params: unknown): TaskEither<AppError, number> => {
  const ParamsType = t.type({ country: t.string });
  return pipe(
    fromEither(decode(params, ParamsType.decode, 'unrecognized countryPopulation params')),
    flatMap(({ country }) => countryPopulation(country)),
  );
};

export const CountryPopulationSchema: OpenAI.FunctionDefinition = {
  name: 'countryPopulation',
  description: `Provides information about the country's population.`,
  parameters: {
    type: 'object',
    properties: {
      country: {
        type: 'string',
        description: 'country code in ISO 3166-1 alpha-3 format',
      },
    },
    required: ['country'],
  },
};
