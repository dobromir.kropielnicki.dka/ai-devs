import { OpenAI } from 'openai';
import { flatMap, fromEither, of, TaskEither } from 'fp-ts/TaskEither';
import { AppError, appErrorTE } from '@core/types.ts';
import { pipe } from 'fp-ts/function';
import { tryCatch } from '@utils/fpHelpers.ts';
import * as t from 'io-ts';
import { decode } from '@utils/decode.ts';
import { isNotEmpty } from '@utils/string.ts';

const openai = new OpenAI();

export const genericQuestion = (question: string): TaskEither<AppError, string> => {
  console.log(`asking llm: ${question}`);

  const params: OpenAI.Chat.ChatCompletionCreateParams = {
    messages: [{ role: 'user', content: question }],
    model: 'gpt-3.5-turbo',
  };

  return pipe(
    tryCatch(() => openai.chat.completions.create(params), 'answering question'),
    flatMap(response => takeFirstMessage(response.choices)),
    flatMap(message =>
      isNotEmpty(message.content) ? of(message.content) : appErrorTE('empty message'),
    ),
  );
};

const takeFirstMessage = (
  choices: OpenAI.Chat.ChatCompletion.Choice[],
): TaskEither<AppError, OpenAI.ChatCompletionMessage> => {
  return choices.length > 0 ? of(choices[0].message) : appErrorTE('no choices');
};

export const tryGenericQuestion = (params: unknown): TaskEither<AppError, string> => {
  const ParamsType = t.type({ question: t.string });
  return pipe(
    fromEither(decode(params, ParamsType.decode, 'unrecognized generic question params')),
    flatMap(({ question }) => genericQuestion(question)),
  );
};

export const GenericQuestionSchema: OpenAI.FunctionDefinition = {
  name: 'genericQuestion',
  description: 'Provides an answer to a generic question based on well-known knowledge.',
  parameters: {
    type: 'object',
    properties: {
      question: {
        type: 'string',
        description: 'The question we are looking for an answer to',
      },
    },
    required: ['question'],
  },
};
