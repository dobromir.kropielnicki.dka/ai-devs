import { QdrantClient } from '@qdrant/js-client-rest';
import { map, TaskEither } from 'fp-ts/TaskEither';
import { AppError } from '@core/types.ts';
import { tryCatch } from '@utils/fpHelpers.ts';
import { pipe } from 'fp-ts/function';

const qdrantUrl = process.env.QDRANT_URL ?? 'http://localhost:6333';
const client = new QdrantClient({ url: qdrantUrl });

export const recreateCollection = (name: string): TaskEither<AppError, boolean> => {
  console.log(`recreate collection ${name}`);
  return pipe(
    tryCatch(
      () =>
        client.recreateCollection(name, {
          vectors: { size: 1536, distance: 'Cosine', on_disk: true },
        }),
      'recreate collection',
    ),
    map(response => response.data.result ?? false),
  );
};

export type CollectionUpsertResult = {
  operation_id: number;
  status: 'acknowledged' | 'completed';
};

export type CollectionEntry = { id: string; vector: number[]; payload: Record<string, unknown> };
export const batchUpsert = (
  name: string,
  entries: CollectionEntry[],
): TaskEither<AppError, CollectionUpsertResult> => {
  console.log(`batch upsert ${entries.length} entries to ${name}`);

  const ids = entries.map(entry => entry.id);
  const vectors = entries.map(entry => entry.vector);
  const payloads = entries.map(entry => entry.payload);

  return tryCatch(
    () =>
      client.upsert(name, {
        batch: { ids, vectors, payloads },
        wait: true,
      }),
    'batch upsert',
  );
};

export type SearchResult<T> = {
  id: string;
  version: number;
  score: number;
  payload: T;
  vector: number[];
};

export const searchCollection = <T>(
  name: string,
  vector: number[],
  options: { limit: number },
): TaskEither<AppError, SearchResult<T>[]> => {
  console.log(`search collection ${name}`);
  return pipe(
    tryCatch(() => client.search(name, { vector, ...options }), 'recreate collection'),
    map(response => response as SearchResult<T>[]),
  );
};
