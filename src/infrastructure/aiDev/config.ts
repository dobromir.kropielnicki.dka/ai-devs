const { API_KEY, API_URL } = process.env;
if (!API_KEY || !API_URL) {
  console.error('API_KEY and API_URL must be set');
  process.exit(-1);
}

export const config = {
  apiKey: API_KEY,
  apiUrl: API_URL,
};
