import { flatMap, fromEither, TaskEither } from 'fp-ts/TaskEither';
import { AppError } from '@core/types.ts';
import { pipe } from 'fp-ts/function';
import { httpPost } from '@utils/httpRequest.ts';
import { decode } from '@utils/decode.ts';
import { Validation } from 'io-ts';
import { config } from './config.ts';

export const postTaskData = <T, K>(
  payload: T,
  options: {
    token: string;
    formData?: boolean;
    validator: (val: unknown) => Validation<K>;
  },
): TaskEither<AppError, K> =>
  pipe(
    httpPost(`${config.apiUrl}/task/${options.token}`, payload, {
      name: 'post-task-api',
      formData: options.formData ?? false,
    }),
    flatMap(result => fromEither(decode(result, options.validator))),
  );
