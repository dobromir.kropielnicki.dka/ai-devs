import { flatMap, map, fromEither, TaskEither, tryCatch } from 'fp-ts/TaskEither';
import { appError, AppError } from '@core/types.ts';
import { pipe } from 'fp-ts/function';
import { left, right } from 'fp-ts/Either';
import * as t from 'io-ts';
import { httpGet } from '@utils/httpRequest.ts';
import { config } from '@infrastructure/aiDev/config.ts';
import { auth } from '@infrastructure/aiDev/auth.ts';
import { log } from '@utils/fpHelpers.ts';

export const getTaskByName = (
  name: string,
): TaskEither<AppError, { token: string; task: unknown }> => {
  return pipe(
    auth(name),
    log('auth'),
    flatMap(token =>
      pipe(
        getTask(token),
        map(task => ({ token, task })),
      ),
    ),
  );
};

export const getTask = (token: string): TaskEither<AppError, unknown> => {
  return pipe(
    httpGet(`${config.apiUrl}/task/${token}`, { name: 'get-task-api' }),
    flatMap(result =>
      ErrorType.is(result) && result.code < 0
        ? fromEither(left(appError(result.msg, result)))
        : fromEither(right(result)),
    ),
  );
};

const ErrorType = t.type({ code: t.number, msg: t.string });
