import * as t from 'io-ts';
import { flatMap, fromEither, TaskEither } from 'fp-ts/TaskEither';
import { AppError, appError } from '@core/types.ts';
import { pipe } from 'fp-ts/function';
import { Either, left, right } from 'fp-ts/Either';
import { httpPost } from '@utils/httpRequest.ts';
import { decode } from '@utils/decode.ts';
import { config } from './config.ts';

export const auth = (task: string): TaskEither<AppError, string> =>
  pipe(
    httpPost(`${config.apiUrl}/token/${task}`, { apikey: config.apiKey }, { name: 'auth-api' }),
    flatMap(response => fromEither(decode(response, AuthResponseType.decode))),
    flatMap(result => fromEither(getToken(result))),
  );

const getToken = (value: AuthResponse): Either<AppError, string> =>
  value.token !== undefined ? right(value.token) : left(appError('token not found', value));

const AuthResponseType = t.intersection([
  t.type({ code: t.number, msg: t.string }),
  t.partial({ token: t.string }),
]);

type AuthResponse = t.TypeOf<typeof AuthResponseType>;
