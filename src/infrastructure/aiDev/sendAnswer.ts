import * as t from 'io-ts';
import { flatMap, fromEither, TaskEither, filterOrElse } from 'fp-ts/TaskEither';
import { pipe } from 'fp-ts/function';

import { appError, AppError } from '@core/types.ts';
import { httpPost } from '@utils/httpRequest.ts';
import { decode } from '@utils/decode.ts';
import { config } from './config.ts';

const AnswerResponseType = t.type({ code: t.number, msg: t.string });
export type AnswerResponse = t.TypeOf<typeof AnswerResponseType>;

export const sendAnswer = <T>(payload: T, token: string): TaskEither<AppError, AnswerResponse> =>
  pipe(
    httpPost(`${config.apiUrl}/answer/${token}`, { answer: payload }, { name: 'answer-api' }),
    flatMap(result => fromEither(decode(result, AnswerResponseType.decode))),
    filterOrElse(
      ({ code }) => code >= 0,
      result => appError('answer is not correct', result),
    ),
  );
