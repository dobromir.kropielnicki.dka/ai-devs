import * as t from 'io-ts';
import { flatMap, TaskEither } from 'fp-ts/TaskEither';
import { AppError } from '@core/types.ts';
import { pipe } from 'fp-ts/function';
import { tryCatch } from '@utils/fpHelpers.ts';
import { getJson } from 'serpapi';
import { parseTE } from '@utils/decode.ts';

const GoogleResult = t.type({
  organic_results: t.array(t.type({ link: t.string })),
});
export type GoogleResult = t.TypeOf<typeof GoogleResult>;

export const googleIt = (
  query: string,
  actionName = 'google it',
): TaskEither<AppError, GoogleResult> => {
  const searchParams = {
    api_key: process.env.SERP_API_KEY,
    engine: 'google',
    q: query,
    location: 'Poland',
  };
  return pipe(
    tryCatch(() => getJson(searchParams), actionName),
    flatMap(result => parseTE(result, GoogleResult.decode, `${actionName} - google response`)),
  );
};
