import { map, TaskEither } from 'fp-ts/TaskEither';
import { pipe } from 'fp-ts/function';
import { OpenAIEmbeddings } from 'langchain/embeddings/openai';
import { AppError } from '@core/types.ts';
import { tryCatch } from '@utils/fpHelpers.ts';

const embeddingClient = new OpenAIEmbeddings({ maxConcurrency: 5, maxRetries: 5 });

export const embeddingQuery = (query: string): TaskEither<AppError, number[]> => {
  console.log(`query embedding: ${query}`);
  return tryCatch(() => embeddingClient.embedQuery(query), 'query embedding');
};

export type WithEmbedding<T> = T & { embedding: number[] };
export const enhanceWithEmbeddings = <T>(
  knowledge: T[],
  toText: (item: T) => string,
): TaskEither<AppError, WithEmbedding<T>[]> => {
  console.log(`enhance with embeddings ${knowledge.length} pieces of knowledge`);
  const texts = knowledge.map(toText);
  return pipe(
    tryCatch<number[][]>(() => embeddingClient.embedDocuments(texts), 'embeddings'),
    map(result => result.map((embedding, index) => ({ ...knowledge[index], embedding }))),
  );
};
