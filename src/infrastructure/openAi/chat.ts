import { flatMap, fromNullable, map, of, TaskEither } from 'fp-ts/TaskEither';
import { pipe } from 'fp-ts/function';
import { BaseMessageLike } from 'langchain/schema';
import { ChatOpenAI } from 'langchain/chat_models/openai';

import { appError, AppError, appErrorTE } from '@core/types.ts';
import { tryCatch } from '@utils/fpHelpers.ts';
import { ChatPromptTemplate } from 'langchain/prompts';
import { LLMChain } from 'langchain/chains';
import { isNotEmpty } from '@utils/string.ts';
import { OpenAI } from 'openai';

export const longChainCall = (
  messages: BaseMessageLike[],
  actionName: string,
): TaskEither<AppError, string> => {
  const llm = new ChatOpenAI({ modelName: 'gpt-3.5-turbo' });
  const prompt = ChatPromptTemplate.fromMessages(messages);
  const longChain = new LLMChain({ llm, prompt });
  return pipe(
    tryCatch(() => longChain.call({}), actionName),
    map(({ text }) => text),
  );
};

const openai = new OpenAI();
export const openAiChatCall = (
  params: OpenAI.Chat.ChatCompletionCreateParamsNonStreaming,
  actionName: string,
): TaskEither<AppError, string> => {
  return pipe(
    tryCatch(() => openai.chat.completions.create(params), actionName),
    flatMap(({ choices: [head, ..._] }) => fromNullable(appError('no message'))(head?.message)),
    flatMap(message =>
      isNotEmpty(message.content) ? of(message.content) : appErrorTE('empty message'),
    ),
  );
};

export const systemMessage = (content: string): OpenAI.Chat.ChatCompletionSystemMessageParam => ({
  role: 'system',
  content,
});

export const userMessage = (
  content: string | OpenAI.Chat.ChatCompletionContentPart[],
): OpenAI.Chat.ChatCompletionUserMessageParam => ({
  role: 'user',
  content,
});
