import { flatMap, fromEither, of, TaskEither } from 'fp-ts/TaskEither';
import { AppError, appErrorTE } from '@core/types.ts';
import { ChatOpenAI } from 'langchain/chat_models/openai';
import { pipe } from 'fp-ts/function';
import { tryCatch } from '@utils/fpHelpers.ts';
import { BaseMessageLike } from 'langchain/schema';
import { decode } from '@utils/decode.ts';
import * as t from 'io-ts';
import { OpenAI } from 'openai';
import { isNotEmpty } from '@utils/string.ts';
import { isNotNil } from '@utils/nil.ts';
import { isRecord } from '@utils/record.ts';

export type FunctionCallInfo = { name: string; args?: unknown };
export type DirectAnswer = string;
export type FunctionCallResult = FunctionCallInfo | DirectAnswer;

export const isFunctionInfoCall = (result: FunctionCallResult): result is FunctionCallInfo => {
  return isRecord(result) && isNotEmpty(result.name);
};
export const isDirectAnswer = (result: FunctionCallResult): result is FunctionCallInfo => {
  return t.string.is(result);
};

export const callForFunction = (
  messages: BaseMessageLike[],
  schemes: OpenAI.FunctionDefinition[],
): TaskEither<AppError, FunctionCallResult> => {
  const llm = new ChatOpenAI({
    modelName: 'gpt-3.5-turbo-1106',
    maxTokens: 1000,
  }).bind({ functions: schemes });

  return pipe(
    tryCatch(() => llm.invoke(messages), 'picking action'),
    flatMap(result => fromEither(decode(result, ResponseType.decode))),
    flatMap(result => {
      switch (true) {
        case isNotEmpty(result.content):
          return of(result.content as DirectAnswer);
        case isNotNil(result.additional_kwargs?.function_call):
          return of(toFunctionCallInfo(result.additional_kwargs.function_call!));
        default:
          return appErrorTE('unknown action', result);
      }
    }),
  );
};

const FunctionInfoType = t.type({ name: t.string, arguments: t.string });
type FunctionInfo = t.TypeOf<typeof FunctionInfoType>;
const ResponseType = t.type({
  content: t.union([t.string, t.null]),
  additional_kwargs: t.partial({ function_call: FunctionInfoType }),
});

const toFunctionCallInfo = (info: FunctionInfo): FunctionCallInfo => ({
  name: info.name,
  args: JSON.parse(info.arguments),
});
