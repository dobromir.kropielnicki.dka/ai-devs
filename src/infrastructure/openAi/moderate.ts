import { taskEither as TE } from 'fp-ts';
import { AppError } from '@core/types.ts';
import { pipe } from 'fp-ts/function';
import { tryCatch } from '@utils/fpHelpers.ts';
import { openAiClient } from './client.ts';

export const moderate = (input: string): TE.TaskEither<AppError, boolean> => {
  const actionName = 'moderation-openai-api';
  return pipe(
    tryCatch(() => openAiClient.moderations.create({ input }), actionName),
    TE.map(({ results }) => results.some(value => value.flagged)),
  );
};
