import { Elysia } from 'elysia';
import { ownApiController } from '@infrastructure/restApi/ownApiController.ts';
import { googleController } from '@infrastructure/restApi/googleController.ts';
import { md2htmlController } from '@infrastructure/restApi/md2htmlController.ts';

const Port: number = +(process.env.PORT || 8081);
const ApiSecret = process.env.DK_API_SECRET ?? 'dk';

const app = new Elysia()
  .get('/', () => 'dk AI Devs 0.0.1')
  .group(`/${ApiSecret}`, app =>
    app.use(ownApiController).use(googleController).use(md2htmlController),
  )
  .listen(Port);

console.log(`🦊 AI Devs is running at ${app.server?.hostname}:${app.server?.port}`);
